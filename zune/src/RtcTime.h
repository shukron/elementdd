#ifndef RTCTIME_H
#define RTCTIME_H

/**
 * RtcTime: Opaque type for RTC Time struct
 */
typedef struct RtcTime RtcTime;

/**
 * Allocate a new instance of RtcTime and initialize it.
 */
RtcTime *RtcTime_Create(int daysSince1980);
void RtcTime_Destroy(RtcTime *self);
int RtcTime_GetYear(RtcTime *self);
int RtcTime_GetMonth(RtcTime *self);
int RtcTime_GetDayOfMonth(RtcTime *self);
int RtcTime_GetDayOfWeek(RtcTime *self);

int isLeapYear(int year);

enum Weekday
{
    Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday
};

enum
{
    STARTING_YEAR = 1980, STARTING_WEEKDAY = Tuesday
};

static const int nonLeapYearDaysPerMonth[12] =
{ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

static const int leapYearDaysPerMonth[12] =
{ 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

#endif /* RTCTIME_H */