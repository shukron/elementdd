/***
 * Excerpted from "Test-Driven Development for Embedded C",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material, 
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose. 
 * Visit http://www.pragmaticprogrammer.com/titles/jgade for more book information.
***/

#include "RtcTime.h"

#include <stdlib.h>

struct RtcTime
{
    int daysSince1980;
    int year;
    int dayOfYear;
    int month;
    int dayOfMonth;
    int dayOfWeek;
};

int isLeapYear(int year)
{
    if (year % 400 == 0)
        return 1;
    if (year % 100 == 0)
        return 0;
    if (year % 4 == 0)
        return 1;
    return 0;
}

static int GetDaysInYear(int year)
{
    if (isLeapYear(year))
        return 366;
    else
        return 365;
}

static void initRtcTime(RtcTime *self, int daysSince1980)
{
    self->daysSince1980 = daysSince1980;
    int days = self->daysSince1980;
    int year = STARTING_YEAR;
    int month = 0;
    const int * daysPerMonth = nonLeapYearDaysPerMonth;
    int daysInYear = GetDaysInYear(year);

    while (days > daysInYear)
    {
        days -= daysInYear;
        year += 1;
        daysInYear = GetDaysInYear(year);
    }

    self->dayOfYear = days;
    self->year = year;

    if (isLeapYear(self->year))
        daysPerMonth = leapYearDaysPerMonth;

    while (days > daysPerMonth[month])
    {
        days -= daysPerMonth[month];
        month++;
    }
     self->month = month + 1;
     self->dayOfMonth = days;

     self->dayOfWeek =  ((self->daysSince1980 - 1) + STARTING_WEEKDAY) % 7;
}


RtcTime *RtcTime_Create(int daysSince1980)
{
     RtcTime *self = (RtcTime *)calloc(1, sizeof(RtcTime));
     initRtcTime(self, daysSince1980);
     return self;
}

void RtcTime_Destroy(RtcTime *self)
{
    free(self);
}

int RtcTime_GetYear(RtcTime *self)
{
   return self->year;
}

int RtcTime_GetMonth(RtcTime *self)
{
    return self->month;
}

int RtcTime_GetDayOfMonth(RtcTime *self)
{
    return self->dayOfMonth;
}

int RtcTime_GetDayOfWeek(RtcTime *self)
{
    return self->dayOfWeek;
}