# ElemenTDD

Code examples about TDD for Team Element

## Getting Started

### Prerequisites

In order to build the examples and run the tests in this repository you will
need:

1. `CMake` 3.11 or newer
2. gnu `make`
3. `CppUTest` 3.+
4. C/C++ compiler (either `clang` or `gcc` will be fine)

Alternatively, you can use the docker image I made, [`shukron/embedded:0.3.0`](https://cloud.docker.com/repository/docker/shukron/embedded)
which contains everything you need to compile and run the code.

## Running the tests
Asuming you have everything installed, all you have to do is:
```bash
cd ElemenTDD

# Create a build directory
mkdir build
cd build

# Configure the project
cmake ..

# Build everything
cmake --build .

# Run the tests
ctest
```

## Built With

* [CMake](https://cmake.org/) - The most popular build-system generator for C++
* [CppUTest](https://cpputest.github.io/) - Test-harness for C/C++

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Authors

* **Avi Shukron** - *Initial work*

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Inspired by James Grenning's book [Test-Driven Development for Embedded C](https://www.amazon.com/Driven-Development-Embedded-Pragmatic-Programmers/dp/193435662X)
