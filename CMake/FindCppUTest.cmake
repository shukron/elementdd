
find_package(PkgConfig)
pkg_check_modules(PC_CppUTest cpputest)

find_path(CppUTest_INCLUDE_DIR CppUTest/TestHarness.h
    HINTS ${PC_CppUTest_INCLUDEDIR} ${PC_CppUTest_INCLUDE_DIRS})

find_library(CppUTest_LIBRARY NAMES CppUTest
    HINTS ${PC_CppUTest_LIBDIR} ${PC_CppUTest_LIBRARY_DIRS})
find_library(CppUTest_EXT_LIBRARY NAMES CppUTestExt
    HINTS ${PC_CppUTest_LIBDIR} ${PC_CppUTest_LIBRARY_DIRS})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
        CppUTest
        DEFAULT_MSG
        CppUTest_LIBRARY CppUTest_EXT_LIBRARY
        CppUTest_INCLUDE_DIR)

if(CppUTest_FOUND)
    set(CppUTest_LIBRARIES ${CppUTest_LIBRARY} ${CppUTest_EXT_LIBRARY})
    set(CppUTest_INCLUDE_DIRS ${CppUTest_INCLUDE_DIR})

    if(NOT TARGET CppUTest)
        add_library(CppUTest UNKNOWN IMPORTED)
        set_target_properties(CppUTest PROPERTIES
                IMPORTED_LOCATION "${CppUTest_LIBRARY}"
                INTERFACE_COMPILE_OPTIONS "${PC_CppUTest_CFLAGS_OTHER}"
                INTERFACE_INCLUDE_DIRECTORIES "${CppUTest_INCLUDE_DIR}")
    endif()
    if(NOT TARGET CppUTestExt)
        add_library(CppUTestExt UNKNOWN IMPORTED)
        set_target_properties(CppUTestExt PROPERTIES
                IMPORTED_LOCATION "${CppUTest_EXT_LIBRARY}"
                INTERFACE_COMPILE_OPTIONS "${PC_CppUTest_CFLAGS_OTHER}"
                INTERFACE_INCLUDE_DIRECTORIES "${CppUTest_INCLUDE_DIR}")
    endif()
endif()

mark_as_advanced(CppUTest_INCLUDE_DIR CppUTest_LIBRARY CppUTest_EXT_LIBRARY)
