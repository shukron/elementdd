cmake_minimum_required(VERSION 3.11.0)

project("ElemenTDD" LANGUAGES C)

LIST(APPEND CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}/CMake")

enable_testing()
add_subdirectory(zune)
